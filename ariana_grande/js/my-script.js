function pageEvents(){
    this.initHeader = (selector, stringHTML) => {
        $(selector).append(stringHTML);     
    }; 
    
    this.initScriptTag = () => {
        let tag = document.createElement('script');
        tag.src = './js/data.js';
        let firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    };

    this.initScrollToTop = (selector) => {
        let _goTopEl = $(selector);
        _goTopEl.css({'display':'none'});
        $(window).scroll(function() {
            if ($(this).scrollTop() > window.innerHeight) {
                _goTopEl.fadeIn();
            } else {
                _goTopEl.fadeOut();
            }
        });

        _goTopEl.click( function(e) {
            $('html, body').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    };

    this.initClickScroll = (selector) => { // add click scroll effect
        $(selector).click( function(e) {
        let target = $($(this).attr('href'));
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - 60
                }, 500);
                return false;
            }
        });
    };

    this.initNavScroll = (selector) => {
        $(window).scroll(function() {
            if ($(window).scrollTop() > 40) {
                $(selector).addClass("scroll");
            } else {
                $(selector).removeClass("scroll");
            }
        })
        let topMenu = $(selector),
            topMenuHeight = topMenu.outerHeight(),
            // All list items
            menuItems = topMenu.find(".navbar-nav a"),
            // Anchors corresponding to menu items
            scrollItems = menuItems.map(function(i, o) {
                let item = $($(o).attr("href"));
                if (item.length) {
                    return item;
                }
            });

        $(window).scroll(function() {
            //Hightlight menu
            let fromTop = $(this).scrollTop() + topMenuHeight;

            // Get id of current scroll item
            let cur = scrollItems.map(function() {
                if ($(this).offset().top < fromTop)
                    return this;
            });

            if(cur == 'undefined') {
                return;
            }

            // Get the id of the current element
            cur = cur[cur.length - 1];
            let id = cur && cur.length ? cur[0].id : "";
            // Set or remove active class
            menuItems
                .removeClass("active")
                .filter('[href="#' + id + '"]').addClass("active");            
        });
    };

    this.initSlickSliders = (slickSliderList) => {
        let sliderName = '';
        if(slickSliderList.length > 0) {
            for(let i=0; i<slickSliderList.length; i++) {
                sliderName = Object.keys(slickSliderList[i])[0];
                $('.'+sliderName).slick(slickSliderList[i][sliderName]);
            }
        }
    };

    this.initlightGallery = (selector) => {
        $(selector).lightGallery(); 
    },

    this.loadYouTubeIframeAPI = () => {
        let tag = document.createElement('script');
        tag.src = 'https://www.youtube.com/iframe_api';
        let firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    };

    this.initIframeYouTube = (selector) => {
        $(selector).next().hide();
        $(selector).click( function(){
            $(this).hide();
            let ifrm = $(this).next(); 
            ifrm.show();            
            ifrm.attr('src', ifrm.attr('src') + '?autoplay=1'); 
        });
    };

    this.initOpenVideo = (selector) => {
        $(selector).on('click', function() { // use Fancy library
            $.fancybox.open({
                src  : 'https://www.youtube.com/embed/zq8EIYyF-4E',
                type : 'iframe',
                opts : {
                    afterShow : function( instance, current ) {
                        console.info( 'done!' );
                    }
                }
            });
        });
    };  
    
    this.initToolTip = (selector) => {
        $(selector).tooltip();   
    };

    this.initCountdown = (selector, endDate) => {
        $(selector).countdown(endDate) //ref: http://hilios.github.io/jQuery.countdown/documentation.html
        .on('update.countdown', function(event) {
            $(selector+' .block:eq(0) .block__num').html(event.strftime('%D'));
            $(selector+' .block:eq(1) .block__num').html(event.strftime('%H'));
            $(selector+' .block:eq(2) .block__num').html(event.strftime('%M'));
            $(selector+' .block:eq(3) .block__num').html(event.strftime('%S'));
            // $('.count-down').countdown('stop');
           
        })
        .on('finish.countdown', function(event) {
		  $(this).html('<h3>Time is up!</h3>')
		    .parent().addClass('disabled');

		})
        .countdown('start');  
    };

    this.initPersonEffect = (containerSelector, targetSelector) => { // ref: TweenMax
       
        let $target = $(targetSelector),
        $container = $(containerSelector),
        container_w = $container.width(),
        container_h = $container.height();

        $(window).on('mousemove.parallax', function(event) {
          let pos_x = event.pageX,
              pos_y = event.pageY,
              left  = 0,
              top   = 0;

          left = container_w / 2 - pos_x;
          top  = container_h / 2 - pos_y;
          
          TweenMax.to(
            $target, 
            5, 
            { 
              css: { 
                transform: 'translateX(' + left / 8 + 'px)' 
              }, 
              ease:Expo.easeOut, 
              overwrite: 'all' 
            });

        });
    };
}

$(() => { // short-hand for: $(document).ready(function() { ... });
    let myPage = new pageEvents();
    myPage.initScriptTag();
    window.onload = function() {
        myPage.initHeader('#header', headerHTML);
        myPage.initScrollToTop('[data-action="gotop"]');
        myPage.initClickScroll('.js-smooth-scroll');
        myPage.initNavScroll('.navbar');
        myPage.initSlickSliders(slickSliderList);
        myPage.initlightGallery('.js-featured-images-slider .slick-track');
        myPage.initIframeYouTube('#thumb-video');   
        myPage.initOpenVideo('#fancy_video');
        myPage.initToolTip('[data-toggle="tooltip"]');
        myPage.initCountdown('.count-down', '2025/01/01 23:59:59');
        myPage.initPersonEffect('.js-person__container', '.js-person');
    };
});

