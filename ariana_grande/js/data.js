var headerHTML = `
<div class="container-fluid">
<a class="d-inline-block" href="#" title="Brand logo">
    <img class="h-100" src="./img/ariana_grande_text.png" alt="Brand logo"/>
</a>
</div>
`;


var slickSliderList = [
    {
        'js-featured-images-slider': {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            // fade: true,
            touchMove: false, 
            swipe: false,
            autoplay: true,
            autoplaySpeed: 2000,
            prevArrow: '<a class="custom-arrow slick-prev has-bg-image"><i class="ello-angle-left"></i></a>',
            nextArrow: '<a class="custom-arrow slick-next has-bg-image"><i class="ello-angle-right"></i></a>'
        }
    },
];